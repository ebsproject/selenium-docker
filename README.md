**TODO**: Finish documentation

**Versions**

Google Chrome 80.0.3987.106

ChromeDriver 80.0.3987.106

Mozilla Firefox 73.0

geckodriver 0.26.0

selenium-side-runner 3.16.0

**Pre-deployment**

git clone https://gitlab.com/ebs_01/ebs-tests.git

git clone https://gitlab.com/ebs_01/ebs-selenium.git

cd ebs-selenium

Edit the 'env' file accordingly.

**Deployment**

bash deploy.sh

**Validation**

curl http://localhost:4444/wd/hub/status

curl http://localhost:4445/wd/hub/status

bash test-chrome.sh

bash test-firefox.sh

NOTE An output directory is not specified in these scripts as they only to confirm if the setup is working.

**Debug**

Debug variant of the standalone images were added to visually see what the browser is doing.

cd debug

bash deploy-debug.sh

Open VNC Viewer and connect to Standalone-Chrome-Debug on port 5900.

Open VNC Viewer and connect to Standalone-Firefox-Debug on port 5901.

NOTE When prompted for the password it is *secret*.

Validate to see if the set up is working:

bash test-chrome-debug-TS001.sh

bash test-firefox-debug-TS001.sh

NOTE An output directory is not specified in these scripts as they only to confirm if the setup is working.

**Reference**

https://github.com/SeleniumHQ/docker-selenium
