#!/bin/bash

docker exec -ti selenium-side-runner-debug bash -c 'selenium-side-runner \
    --server http://chrome-debug:4444/wd/hub  \
    -c "browserName='chrome'" \
    --output-format=junit /home/seluser/sides/Functional_Testing/B4R-Microsoft_Authentication/initial_authentication.side'
