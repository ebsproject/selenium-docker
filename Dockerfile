FROM ubuntu:latest

RUN apt-get update -yq \
  && apt-get install curl gnupg iputils-ping jq sudo vim -yq \
  && curl -sL https://deb.nodesource.com/setup_10.x | bash \
  && apt-get install nodejs -yq

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update -yq \
  && apt-get install yarn -yq \
  && yarn global add selenium-side-runner

RUN useradd seluser \
         --shell /bin/bash  \
         --create-home \
  && usermod -a -G sudo seluser \
  && echo 'ALL ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers \
  && echo 'seluser:secret' | chpasswd \
  && mkdir /home/seluser/selenium \
  && mkdir /home/seluser/sides
ENV HOME=/home/seluser

USER seluser

WORKDIR /home/seluser/selenium

CMD tail -f /dev/null
